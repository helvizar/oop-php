<?php

require_once('animal.php');
require('ape.php');
require('frog.php');

$sheep = new animal("shaun");

echo "Nama = " . $sheep->name . "<br>";
echo "legs = " . $sheep->legs . "<br>";
echo "cold blooded = " . $sheep->cold_blooded . "<br>";
echo "<br>";

$sungokong = new Ape("kera sakti");

echo "Nama = " . $sungokong->name . "<br>";
echo "legs = " . $sungokong->legs . "<br>";
echo "cold blooded = " . $sungokong->cold_blooded . "<br>";
echo "Yell = " . $sungokong->yell . "<br>";
echo "<br>";

$kodok = new Frog("buduk");

echo "Nama = " . $kodok->name . "<br>";
echo "legs = " . $kodok->legs . "<br>";
echo "cold blooded = " . $kodok->cold_blooded . "<br>";
echo "Jump = " . $kodok->jump . "<br>";
echo "<br>";

?>